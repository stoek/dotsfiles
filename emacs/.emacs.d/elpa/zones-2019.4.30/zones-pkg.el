;;; -*- no-byte-compile: t -*-
(define-package "zones" "2019.4.30" "Zones of text - like multiple regions" 'nil :url "https://elpa.gnu.org/packages/zones.html" :keywords '("narrow" "restriction" "widen" "region" "zone"))
